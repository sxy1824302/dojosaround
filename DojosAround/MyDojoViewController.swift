//
//  MyDojoViewController.swift
//  DojosAround
//
//  Created by alexander sun on 11/12/15.
//  Copyright © 2015 The Arsonist. All rights reserved.
//

import Foundation
import UIKit
import Parse
import PKHUD
import Eureka
class DojoPicturesCell: UITableViewCell {
    @IBOutlet weak var dojoLogoView: UIImageView!
    @IBOutlet weak var importPhotoView: UIView!
    
    @IBAction func importClicked(sender: AnyObject) {
        
    }
}
class TitleInfoCell: UITableViewCell {
    @IBOutlet weak var valueLabel: UILabel!
    
}
class ClassInfoCell: UITableViewCell {
    
    @IBOutlet weak var classInfoLabel: UILabel!
}

class MyDojoViewController: UITableViewController {

    
    struct CellIdentifier {
        static let DojoPicturesCell = "DojoPicturesCell"
        static let TitleInfoCell = "TitleInfoCell"
        static let ClassInfoCell = "ClassInfoCell"
    }
    


    @IBAction func editInfo(sender: AnyObject) {
        let cvc = CreateDojoViewController()
        cvc.dojo  = PFUser.currentUser()?.objectForKey("dojo") as? PFObject
        showViewController(cvc, sender: self)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Dojo"
        
        
        let headerView = ParallaxHeaderView.init(image:UIImage(named: "YourImageName"), forSize: CGSizeMake(self.tableView.frame.size.height, 300))
        self.tableView.tableHeaderView = headerView
        
        let location = UIButton(type: .Custom)
        location.setImage(UIImage(named: "loc"), forState:.Normal)
        location.addTarget(self, action: "changeLoc", forControlEvents: UIControlEvents.TouchUpInside)
        
        location.sizeToFit()
        
        location.frame = CGRectMake(headerView.frame.size.width - location.frame.size.width, headerView.frame.size.height - location.frame.size.height, location.frame.size.width,  location.frame.size.height)
        
        headerView.addSubview(location)
        
        let g = UITapGestureRecognizer(target: self, action: "changePicture:")
        
        headerView.addGestureRecognizer(g)
        
    }
    
    func changeLoc(){
        
        if let dojo = PFUser.currentUser()?.objectForKey("dojo") as? PFObject{
            
            let vc =  EditProfileViewController()
            vc.userInfo = dojo
            vc.buildCallback = {(form)->(BaseRow) in
                return LocationRow() {
                    $0.title = "Location"
                    
                    if let location = dojo.objectForKey("location") as? PFGeoPoint{
                         $0.value =   CLLocation(coordinate: CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude), altitude: 0, horizontalAccuracy: 0, verticalAccuracy: 0, timestamp: NSDate())
                    }
                    
                   
                }
            }
            vc.submitCallback = { (row,info) -> () in
                if let row = row as? LocationRow{
                    if let value = row.value{
                        info.setObject(PFGeoPoint(location: value), forKey: "location")
                    }
                }
            }
            
            
            vc.hidesBottomBarWhenPushed = true
            self.showViewController(vc, sender: self)
        }
    }
    
    func changePicture(gesture:UITapGestureRecognizer){
        
        let vc =  EditProfileViewController()
        
        vc.userInfo = PFUser.currentUser()?.objectForKey("dojo") as? PFObject
        

        vc.buildCallback = {(form)->(BaseRow) in
            return ImageRow(){
                $0.title = "Upload Dojo Picture"
            }
        }
        
        vc.submitCallback = { (row,info) -> () in
            if let row = row as? ImageRow{
                if let value = row.value{
                    
                    if let photo = self.uploadPhoto(value){
                        
                        info.setObject(photo, forKey: "logo")
                    }
                    
                }
            }
        }
        
        vc.hidesBottomBarWhenPushed = true
        self.showViewController(vc, sender: self)
        
    }
    

    override func  scrollViewDidScroll(scrollView: UIScrollView) {
    
        let header = self.tableView.tableHeaderView as! ParallaxHeaderView
        header.layoutHeaderViewForScrollViewOffset(scrollView.contentOffset)
        
        self.tableView.tableHeaderView = header
    }
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        

        
        PKHUD.sharedHUD.contentView = PKHUDTextView(text: "Loading...")
        PKHUD.sharedHUD.show()
        
        
        
        if let dojo = PFUser.currentUser()?.objectForKey("dojo") as? PFObject{
            
            dojo.fetchIfNeededInBackgroundWithBlock({ (dojo, error) -> Void in
                
                
                if let logo = dojo?.objectForKey("logo") as? PFObject{
                    
                    do{
                        try logo.fetchIfNeeded()
                    }catch{
                        
                    }
                    
                    if let thumb = logo.objectForKey("image") as? PFFile{
                        
                        let header = self.tableView.tableHeaderView as! ParallaxHeaderView
                        
                  
                        thumb.getFilePathInBackgroundWithBlock({ (path, error) -> Void in
                            
                            if let path = path{
                               
                                header.headerImage = UIImage(contentsOfFile: path)
                            }
                            
                        })
                    }
                    
                    
                    
                }
                
                
                if error == nil{
                    
                    PKHUD.sharedHUD.hide(animated: true, completion: nil)
                    
                    self.tableView.reloadData()
                    
                }else{
                    
                    PKHUD.sharedHUD.contentView = PKHUDErrorView()
                    
                    PKHUD.sharedHUD.hide(afterDelay: 0.5)
                    
                }
                
            })
        }
        
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard (PFUser.currentUser()?.objectForKey("dojo") as? PFObject)?.dataAvailable ?? false  else{
            return 0
        }
        
        return 5
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

        return  UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

        
        return  UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let dojo = PFUser.currentUser()?.objectForKey("dojo") as? PFObject
        
        switch indexPath.row{
            
        case 0:

            let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier.TitleInfoCell) as? TitleInfoCell
            cell?.valueLabel.text = dojo?.objectForKey("name") as? String
            return cell!
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier.TitleInfoCell) as? TitleInfoCell
     
            if let info = dojo?.objectForKey("school") as? String{
                cell?.valueLabel.text =  info
            }else{
                cell?.valueLabel.text = "Please choose the school of this dojo"
            }
            return cell!
        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier.TitleInfoCell) as? TitleInfoCell
            
            if let info = dojo?.objectForKey("website") as? String{
                cell?.valueLabel.text =  info
                cell?.valueLabel.textColor = UIColor.darkTextColor()
            }else{
                cell?.valueLabel.textColor = UIColor.lightGrayColor()
                cell?.valueLabel.text = "Tap to set your website."
            }
            return cell!
        case 3:
            let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier.TitleInfoCell) as? TitleInfoCell
            
            if let info = dojo?.objectForKey("phone") as? String{
                cell?.valueLabel.text =  info
                cell?.valueLabel.textColor = UIColor.darkTextColor()
            }else{
                cell?.valueLabel.textColor = UIColor.lightGrayColor()
                cell?.valueLabel.text = "Tap to set your dojo's phone."
            }
            return cell!
        case 4:
            let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier.ClassInfoCell) as? ClassInfoCell
            if let info = dojo?.objectForKey("classInformation") as? String{
                cell?.classInfoLabel.text = info
            }else{
                cell?.classInfoLabel.text = "Tap to complete class information"
            }
            
            return cell!
            
        default:
            break
            
        }
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier.TitleInfoCell)
        return cell!

    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        PFUser.currentUser()?.objectForKey("dojo")?.fetchIfNeededInBackgroundWithBlock({ (info, error) -> Void in
            
            if let info = info{
                
                PKHUD.sharedHUD.hide(animated: true, completion: nil)
                
                
                let vc =  EditProfileViewController()
                vc.userInfo = info
                
                switch indexPath.row{
                case 0:
  
                    vc.buildCallback = {(form)->(BaseRow) in
                        return NameRow() {
                            $0.title = "Name"
                            $0.value = info.objectForKey("name")  as? String
                        }
                    }
                    vc.submitCallback = { (row,info) -> () in
                        if let row = row as? NameRow{
                            if let value = row.value{
                                info.setObject(value, forKey: "name")
                            }
                        }
                    }
                case 1:
                    
                    
                    
                    
                    vc.buildCallback = {(form)->(BaseRow) in
                        return PushRow<Schools>() {
                            $0.title = "School"
                            $0.options = Schools.allValues
                            $0.selectorTitle = "Choose a school"
                        }
                    }
                    vc.submitCallback = { (row,info) -> () in
                        if let row = row as? PushRow<Schools>{
                            if let value = row.value{
                                info.setObject(value.rawValue, forKey: "school")
                            }
                        }
                    }
                case 2:
                    vc.buildCallback = {(form)->(BaseRow) in
                        return TextFloatLabelRow(){
                            $0.title = "Website"
                            $0.value = info.objectForKey("website")  as? String
                        }
                    }
                    vc.submitCallback = { (row,info) -> () in
                        if let row = row as? TextFloatLabelRow{
                            if let value = row.value{
                                info.setObject(value, forKey: "website")
                            }
                        }
                    }
                case 3:
                    vc.buildCallback = {(form)->(BaseRow) in
                        return PhoneRow(){
                            $0.title = "Phone"
                            $0.value = info.objectForKey("phone")  as? String
                        }
                    }
                    vc.submitCallback = { (row,info) -> () in
                        if let row = row as? PhoneRow{
                            if let value = row.value{
                                info.setObject(value, forKey: "phone")
                            }
                        }
                    }
                case 4:
                    vc.buildCallback = {(form)->(BaseRow) in
                        return TextAreaRow(){
                            $0.placeholder = "Class Infomation"
                            $0.value = info.objectForKey("classInformation")  as? String
                        }
                    }
                    
                    vc.submitCallback = { (row,info) -> () in
                        if let row = row as? TextAreaRow{
                            if let value = row.value{
                                info.setObject(value, forKey: "classInformation")
                            }
                        }
                    }
                    
                    
                default:
                    break
                }
                
                vc.hidesBottomBarWhenPushed = true
                self.showViewController(vc, sender: self)
                
                
                
            }
        })
    }
    
    
    func uploadPhoto(photo:UIImage)->PFObject?{
        
        let image = photo.resizedImageWithContentMode(.ScaleAspectFit, bounds: CGSizeMake(560, 560), interpolationQuality: .Default)
        
        let thumbnail = image.thumbnailImage(86, transparentBorder: 0, cornerRadius: 0, interpolationQuality: .Default)
        
        let imageData = UIImageJPEGRepresentation(image, 0.8)
        let thumbData = UIImagePNGRepresentation(thumbnail)
        
        guard imageData != nil && thumbData != nil else{
            return nil
        }
        
        let photoFile = PFFile(data: imageData!)
        let thumbFile = PFFile(data: thumbData!)
        
        
        let photo = PFObject(className: "Photo")
        photo["image"] = photoFile
        photo["thumbnail"] = thumbFile
        photo["user"] = PFUser.currentUser()
        
        
        return photo
    }
}
