//
//  CreateDojoViewController.swift
//  DojosAround
//
//  Created by alexander sun on 11/12/15.
//  Copyright © 2015 The Arsonist. All rights reserved.
//

import UIKit
import Eureka
import CoreLocation
import Parse
import Foundation
import CoreLocation
import PKHUD
enum Schools : String, CustomStringConvertible {
    case Aikido = "Aikido"
    case Boxing = "Boxing"
    case BJJ = "Brazilian Jiu Jitsu"
    case Bujinkan = "Bujinkan"
    case Capoeira = "Capoeira"
    case Defendo = "Defendo"
    case Eskrima = "Eskrima"
    case GojuRyuKarate = "Goju Ryu Karate"
    case Laido = "Laido"
    case JeetKuneDo = "Jeet Kune Do"
    case Jujitsu = "Jujitsu"
    case Judo = "Judo"
    case Kempo = "Kempo"
    case kendo = "kendo"
    case KickBoxing = "Kickboxing"
    case Kobudo = "Kobudo"
    case KravMaga = "Krav Maga"
    case KyokushinKarate = "Kyokushin Karate"
    case MuayThai = "Muay Thai"
    case Ninjutsu = "Ninjutsu"
    case Shaolin = "Shaolin kung fu"
    case ShitoRyuKarate = "Shito ryu karate"
    case ShotokanKarate = "Shotokan karate"
    case Systema = "Systema"
    case Taekwondo = "Taekwondo"
    case Taichi = "Tai Chi"
    case WingTsun = "Wing Tsun"
    
    var description : String { return rawValue }
    
    static let allValues = [Aikido, Boxing, BJJ, Bujinkan, Capoeira, Defendo, Eskrima, GojuRyuKarate, Laido,JeetKuneDo,Jujitsu,Judo,Kempo,kendo,KickBoxing,Kobudo,KravMaga,KyokushinKarate,MuayThai,Ninjutsu,Shaolin,ShitoRyuKarate,ShotokanKarate,Systema,Taekwondo,Taichi,WingTsun]
}


class CreateDojoViewController: FormViewController {
    
    
    var dojo:PFObject?
    
    var nameRow:TextFloatLabelRow!
    
    var logoRow:ImageRow!
    
    var schoolRow:PushRow<Schools>!
    
    var locationRow:LocationRow!
    
    var classInfomationRow:TextAreaRow!
    
    var imageChanged:Bool = false
    
    var backgroundTaskIdentifier:UIBackgroundTaskIdentifier?
    
    struct Tags {
        static let name = "Dojo Name"
        static let logo = "Dojo Logo"
        static let school = "Schools"
        static let location = "Located"
    }
    
    
    var submitButton:UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let _ = dojo {
            title = "Edit Your Dojo"
        }else{
            title = "Submit Your New Dojo"
        }
        
        submitButton =  UIBarButtonItem(title: "Submit", style:.Plain, target: self, action: "submitDojo:")
        
        
        navigationItem.rightBarButtonItem = submitButton

        nameRow = TextFloatLabelRow(Tags.name) {
            $0.title = $0.tag
            
            if let name = dojo?.objectForKey("name") {
                $0.value = name as? String
            }
        }
        
        logoRow = ImageRow(Tags.logo) {
            $0.title = $0.tag
            
            if let logo = dojo?.objectForKey("logo") as? PFFile{
                logo.getFilePathInBackgroundWithBlock({ (path, error) -> Void in
                    self.logoRow.value = UIImage(contentsOfFile: path!)
                })
            }
        }.onChange({ (row) -> () in
            self.imageChanged = true
        })
        
        
        schoolRow = PushRow<Schools>(Tags.school) {
            $0.title = $0.tag
            $0.options = Schools.allValues
            $0.selectorTitle = "Choose a school"
        }
        
        locationRow = LocationRow(){
            $0.title = $0.tag
            
            if let location = dojo?.objectForKey("location") as? PFGeoPoint{
                $0.value = CLLocation(coordinate: CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude), altitude: 0, horizontalAccuracy: 0, verticalAccuracy: 0, timestamp: NSDate())
            }
        }
        
        form +++ Section("Dojo infomation")
            <<< nameRow
            <<< logoRow
            <<< schoolRow
            <<< locationRow
        
        
        
        
        
        classInfomationRow = TextAreaRow() {
            $0.placeholder = "Class time,charge,etc"
            
            if let classInformation = dojo?.objectForKey("classInformation") {
                $0.value = classInformation as? String
            }
        }
        
        form +++ Section("Class Infomation")
            <<< classInfomationRow
        

    }
    
    func submitDojo(sender:UIBarButtonItem){
        
        let dojo =  PFObject(className: "Dojo")
        
        var reason:String?
        
        
        if let name = self.nameRow.value{
            
            dojo.setObject(name, forKey: "name")
            

        }
        
        if  let logo = self.logoRow.value{
            
            guard imageChanged else{ return }
            
            let image = logo.resizedImageWithContentMode(.ScaleAspectFit, bounds: CGSizeMake(560, 560), interpolationQuality: .Default)
            
            let thumbnail = image.thumbnailImage(86, transparentBorder: 0, cornerRadius: 0, interpolationQuality: .Default)
   
            let imageData = UIImageJPEGRepresentation(image, 0.8)
            let thumbData = UIImagePNGRepresentation(thumbnail)
            
            guard imageData != nil && thumbData != nil else{
                return
            }
            
            let photoFile = PFFile(data: imageData!)
            let thumbFile = PFFile(data: thumbData!)

            
            let photo = PFObject(className: "Photo")
            photo["image"] = photoFile
            photo["thumbnail"] = thumbFile
            photo["user"] = PFUser.currentUser()
            
            
            dojo.setObject(photo, forKey: "logo")
            
        
        }
        
        if let school = self.schoolRow.value{
            
            dojo.setObject(school.rawValue, forKey: "school")
            
        }else{
            reason = "You must choose at least one school"
            
        }
        
        locationRow = form.rowByTag(Tags.location)
        
        if let location = self.locationRow.value{
            
            dojo.setObject(PFGeoPoint(location: location), forKey: "location")
            
        }else{
            reason = "You must choose a location of your dojo"
            
        }
        
        
     
        if let classInfo = self.classInfomationRow.value{
            dojo.setObject(classInfo, forKey: "classInformation")
        }

        
        if reason == nil{
            
            PKHUD.sharedHUD.contentView = PKHUDTextView(text: "Submitting...")
            PKHUD.sharedHUD.show()
            
            backgroundTaskIdentifier = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler({ () -> Void in
                UIApplication.sharedApplication().endBackgroundTask(self.backgroundTaskIdentifier!)
            })
            PFUser.currentUser()?.setObject(dojo, forKey: "dojo")
            
            PFUser.currentUser()?.saveInBackgroundWithBlock({ (success, error) -> Void in
                if success{
                    
                    self.navigationController?.popToRootViewControllerAnimated(true)
                    
                }
                UIApplication.sharedApplication().endBackgroundTask(self.backgroundTaskIdentifier!)
                PKHUD.sharedHUD.hide(animated: true, completion: nil)
            })
            
            
            
        }else{
            
            
            PKHUD.sharedHUD.contentView = PKHUDTextView(text: reason)
            PKHUD.sharedHUD.show()
            PKHUD.sharedHUD.hide(afterDelay: 2)
        }
        
        
        
    }


}
