//
//  LocationManager.swift
//  DojosAround
//
//  Created by alexander sun on 11/12/15.
//  Copyright © 2015 The Arsonist. All rights reserved.
//

import Foundation
import CoreLocation
import Parse


class LocationManager :NSObject,CLLocationManagerDelegate{
    
    struct AuthorizationStatusNotification {
        static let StatusChange = "AuthorizationStatusNotificationStatusChange"
        static let LocationChanged = "AuthorizationStatusNotificationLocationChanged"
    }
    
    var lastLocation:PFGeoPoint?
    
    static let SharedLocationManager = LocationManager()
    
    let manager = CLLocationManager()
    


    override init(){
        super.init()
        manager.delegate = self
        
        if CLLocationManager.authorizationStatus() != .AuthorizedWhenInUse{
            manager.requestWhenInUseAuthorization()
        }else{
            manager.startUpdatingLocation()
        }
        
    }
    
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        NSNotificationCenter.defaultCenter().postNotificationName(LocationManager.AuthorizationStatusNotification.StatusChange, object: status as? AnyObject, userInfo: nil)
        
        if status == .AuthorizedWhenInUse{
            manager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        if let location = locations.first{
         
            
            if let user = PFUser.currentUser(){
                
                manager.stopUpdatingLocation()
                
                var info =  user.objectForKey("info")
                
                if info == nil {
                    info = PFObject (className: "UserInfo")
                    
                    user.setObject(info!, forKey: "info")
                    
                    user.saveInBackground()
                }
                
                lastLocation = PFGeoPoint(location: location)
                
                NSNotificationCenter.defaultCenter().postNotificationName(LocationManager.AuthorizationStatusNotification.LocationChanged, object: lastLocation, userInfo: nil)
                
                info?.setObject(lastLocation, forKey: "lastLocation")
                
                info?.saveInBackgroundWithBlock {
                    (success: Bool, error: NSError?) -> Void in
                    if (success) {
                        
                    } else {
                        debugPrint(error)
                    }
                }
                
  
            }

            
            
        }
       
    }
    
    
    
}