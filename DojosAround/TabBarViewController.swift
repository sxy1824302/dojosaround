//
//  TabBarViewController.swift
//  DojosAround
//
//  Created by alexander sun on 11/19/15.
//  Copyright © 2015 The Arsonist. All rights reserved.
//

import UIKit
import Parse


class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        updateTabBar()
    }
    
    
    func updateTabBar(){
        
        let user = PFUser.currentUser()

        if (user == nil || !user!.authenticated || PFUser.currentUser()?.objectForKey("dojo") == nil){
            
            if viewControllers?.count > 3{
                viewControllers?.removeAtIndex(2)
            }
            
        }else{
            
            if viewControllers?.count < 4{
                 self.performSegueWithIdentifier("ShowDojoSegue", sender: self)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

class ShowDojoSegue: UIStoryboardSegue {
    override func perform() {
        let tab = self.sourceViewController as! UITabBarController
        
        tab.viewControllers?.insert(self.destinationViewController, atIndex: 2)
        
    }
}
