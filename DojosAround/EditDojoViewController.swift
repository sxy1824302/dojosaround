//
//  EditDojoViewController.swift
//  DojosAround
//
//  Created by alexander sun on 12/16/15.
//  Copyright © 2015 The Arsonist. All rights reserved.
//

import UIKit
import Foundation
import Eureka
import Parse
import PKHUD


class EditDojoViewController: FormViewController {


    var buildCallback : ((Form) -> (BaseRow))?
    
    var submitCallback : ((BaseRow,PFObject) -> ())?
    
    var dojo:PFObject!
    
    var valueRow:BaseRow?
    
    
    
    var submitButton:UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        submitButton =  UIBarButtonItem(title: "Submit", style:.Plain, target: self, action: "submitChange:")
        
        
        navigationItem.rightBarButtonItem = submitButton
        
        
        valueRow = self.buildCallback?(form)
        
        
        form +++ Section() <<< valueRow!
    }
    
    
    func submitChange(sender:UIBarButtonItem){
        
        
        self.submitCallback?(valueRow!,dojo)
        
        PKHUD.sharedHUD.contentView = PKHUDTextView(text: "Submitting...")
        PKHUD.sharedHUD.show()
        
        dojo.saveInBackgroundWithBlock({ (saved, error) -> Void in
            
            if saved{
                self.navigationController?.popToRootViewControllerAnimated(true)
            }
            PKHUD.sharedHUD.hide(animated: true, completion: nil)
            
            
        })
        
    }

}

