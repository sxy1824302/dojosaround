//
//  PictureView.swift
//  DojosAround
//
//  Created by alexander sun on 11/30/15.
//  Copyright © 2015 The Arsonist. All rights reserved.
//

import UIKit
import Parse
import ParseUI
class PictureView: PFImageView {
    
    var placeHolderImage:UIImage? = UIImage(named: "AvatarPlaceholder")
    
    var picture:PFObject?{
        didSet(oldValue){
            picture?.fetchIfNeededInBackgroundWithBlock({ (picture, error) -> Void in
                if let picture = picture{
                    self.image = self.placeHolderImage
                    self.file = picture.objectForKey("thumbnail") as? PFFile
                    self.loadInBackground()
                }
            })
        }
    }
    
}
