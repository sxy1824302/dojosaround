//
//  MapViewController.swift
//  DojosAround
//
//  Created by alexander sun on 11/12/15.
//  Copyright © 2015 The Arsonist. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import CoreLocation
import Parse
class AnnotationButton:UIButton {
    var annotation:DojoAnnotation!
}

class DojoMapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    var dojos:[PFObject]?
    
    let clusteringManager = FBClusteringManager()
    
    var lastQuery:PFQuery?
    
    var tmpDojo:DojoAnnotation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LocationManager.SharedLocationManager
        

        NSNotificationCenter.defaultCenter().addObserverForName(LocationManager.AuthorizationStatusNotification.StatusChange, object: nil, queue: nil) { (notification) -> Void in
            if let status = notification.object as? CLAuthorizationStatus{
                if  status == .AuthorizedWhenInUse{
                  
                    self.locateUser()
                    
                }
            }
            
            
        }

         self.locateUser()
    }

    func locateUser(){
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .Follow
    }
    
    func updatePin(sw:PFGeoPoint,ne:PFGeoPoint){
    
        // Create a query for places
        let query = PFQuery(className:"Dojo")
        // Interested in locations near user.
        query.whereKey("location", withinGeoBoxFromSouthwest: sw, toNortheast: ne)

        debugPrint(query)
        
        query.findObjectsInBackgroundWithBlock({ (objects, error) -> Void in
            
            
            if let q = self.lastQuery{
                guard q == query else {
                    debugPrint("cancel")
                    return
                }
            }

            
            if let objects = objects{
                
                
                
                NSOperationQueue().addOperationWithBlock({
                    
                    self.dojos = objects
                    
                    let annos = self.dojos?.flatMap{DojoAnnotation(aDojo: $0)}
                    
                    self.clusteringManager.setAnnotations(annos!)
                    
                    
                    let mapBoundsWidth = Double(self.mapView.bounds.size.width)
                    
                    let mapRectWidth:Double = self.mapView.visibleMapRect.size.width
                    
                    let scale:Double = mapBoundsWidth / mapRectWidth
                    
                    let annotationArray = self.clusteringManager.clusteredAnnotationsWithinMapRect(self.mapView.visibleMapRect, withZoomScale:scale)
                    
                    self.clusteringManager.displayAnnotations(annotationArray, onMapView:self.mapView)
                })
            }
            
            
            
        })
        
        lastQuery = query
    }
    
    
    
    func annotationDetailClicked(button:AnnotationButton){
       
        tmpDojo = button.annotation
        
         performSegueWithIdentifier("DojoDetailSegue", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let vc = segue.destinationViewController as? DojoDetailTableViewController
        
        vc?.dojo = tmpDojo?.dojo
        
    }
    
}




extension DojoMapViewController : MKMapViewDelegate {
    
    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool){
        
        let mapRect = mapView.visibleMapRect
        
        var pNE = mapRect.origin, pSW = mapRect.origin
        
        pNE.x += mapRect.size.width
        pSW.y += mapRect.size.height
        

        let sw = MKCoordinateForMapPoint(pSW)
        let ne = MKCoordinateForMapPoint(pNE)
        
        updatePin( PFGeoPoint(latitude: sw.latitude, longitude: sw.longitude), ne:  PFGeoPoint(latitude: ne.latitude, longitude: ne.longitude))
        

    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        if !annotation.isKindOfClass(MKUserLocation){
            
            var reuseId = ""
            
            if let dojo = annotation as? DojoAnnotation{
                
                let view = DojoAnnotation.annotation(mapView, annotation: dojo)
                
                let rightButton = AnnotationButton(type: .DetailDisclosure)
                
                rightButton.addTarget(self, action: "annotationDetailClicked:", forControlEvents: .TouchUpInside)
                
                rightButton.annotation = dojo
                
                view.rightCalloutAccessoryView = rightButton
                
                return view
                
            }else if annotation.isKindOfClass(FBAnnotationCluster) {
                reuseId = "Cluster"
                var clusterView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
                clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: reuseId)
                return clusterView
            }
            
        }
        
        return nil
        
    }
    

}




