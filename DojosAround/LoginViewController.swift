//
//  LoginViewController.swift
//  DojosAround
//
//  Created by alexander sun on 11/13/15.
//  Copyright © 2015 The Arsonist. All rights reserved.
//

import UIKit
import ParseUI

class LoginViewController: PFLogInViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        logInView?.logo = UIImageView(image: UIImage(named: "loading-logo"))
        
        signUpController?.signUpView?.logo = UIImageView(image: UIImage(named: "loading-logo"))
        
        signUpController?.emailAsUsername = true

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
