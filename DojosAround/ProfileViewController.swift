//
//  ProfileTableViewController.swift
//  DojosAround
//
//  Created by alexander sun on 11/13/15.
//  Copyright © 2015 The Arsonist. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import ParseUI
import Parse
import PKHUD
import Eureka
class ProfileTableViewController: UITableViewController{
    

    let logoutBtn = UIButton(type: .System)
    
    var user:PFUser?{
        return PFUser.currentUser()
    }
    
    var isDataValid:Bool{
    
        let info = user?.objectForKey("info") as? PFObject
        
        return (user?.authenticated ?? false) && (info?.dataAvailable ?? false)
    }
    
    struct CellConfig {
        static let ProfileCell = "ProfileCell"
        static let NameCell = "BaseCell"
        static let EmailCell = "BaseCell"
        static let PhoneCell = "BaseCell"
        static let BioCell = "BioCell"
    }
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "My Profile"
        
        
        
        self.tableView.emptyDataSetDelegate = self
        self.tableView.emptyDataSetSource = self

 
        
        logoutBtn.setAttributedTitle(NSAttributedString(string: "Logout", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor(),NSFontAttributeName:UIFont.defaultFont(15)!]), forState: .Normal)

        logoutBtn.tintColor = UIColor.whiteColor()
        logoutBtn.frame = CGRectMake(0, 0, tableView.frame.size.width, 44)
        logoutBtn.backgroundColor = UIColor(string: "660000")
        logoutBtn.addTarget(self, action: "logout", forControlEvents: .TouchUpInside)
    }
    

    
    
    func logout(){
        PKHUD.sharedHUD.contentView = PKHUDTextView(text: "Clearing data...")
        PKHUD.sharedHUD.show()
        PFUser.logOutInBackgroundWithBlock { (error) -> Void in
            PKHUD.sharedHUD.hide(animated: true, completion: nil)
            self.reloadUI()
        }

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        reloadUI()
    }
    

    

    
    func reloadUI(){
        
        
        if (user?.authenticated ?? false){
            

            if let dojo = PFUser.currentUser()?.objectForKey("dojo") as? PFObject{
                dojo.fetchIfNeededInBackgroundWithBlock({ (dojo, error) -> Void in
                    let tab = self.tabBarController as? TabBarViewController
                    
                    tab?.updateTabBar()
                })
            }
            
            PKHUD.sharedHUD.contentView = PKHUDTextView(text: "Loading...")
            PKHUD.sharedHUD.show()
            if let info = user?.objectForKey("info") as? PFObject {
                

                
                info.fetchIfNeededInBackgroundWithBlock({ (fectedInfo, error) -> Void in
                    
                    

                    if error == nil{
                        
                        PKHUD.sharedHUD.hide(animated: true, completion: nil)
                        
                        self.tableView.tableFooterView = self.logoutBtn
                        
                        self.tableView.reloadData()
                        
                    }else{
                        
                        PKHUD.sharedHUD.contentView = PKHUDErrorView()
                        PKHUD.sharedHUD.hide(afterDelay: 0.5)
                    }
                    
                })

            }else{
                let info = PFObject (className: "UserInfo")
                
                self.user!.setObject(info, forKey: "info")
                
                self.user!.saveInBackgroundWithBlock({ (success, error) -> Void in
                    
                    if error == nil{
                        
                        PKHUD.sharedHUD.hide(animated: true, completion: nil)
                        
                        self.tableView.tableFooterView = self.logoutBtn
                        
                        self.tableView.reloadData()
                        
                    }else{
                        
                        PKHUD.sharedHUD.contentView = PKHUDErrorView()
                        PKHUD.sharedHUD.hide(afterDelay: 0.5)
                    }
                })
                
            }
            
            
        }else{
            
            self.tableView.reloadData()
            
            self.tableView.tableFooterView = UIView()
    
        }
    }
    

    

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if (isDataValid){
            
            if user?.objectForKey("dojo") == nil{
                
                return 2
                
            }else{
                return 1
            }
        }
        
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (isDataValid){
            
            switch section{
            case 0:
                return 4
            case 1:
                return 1
            default:
                break
            }
            
        }
        return 0
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let info = user?.objectForKey("info") as! PFObject
        
        var cell:UITableViewCell!
    
        if indexPath.section == 0{
            
            switch indexPath.row {
            case 0:
                let acell = tableView.dequeueReusableCellWithIdentifier(CellConfig.ProfileCell, forIndexPath: indexPath) as! ProfileCell
                
                acell.titleLabel.text = "Profile Photo"
                
                if let picture = info.objectForKey("picture") as? PFObject{
                    
                    acell.profileView.picture = picture
                    
                }
                
                cell = acell
                
            case 1:
                
                let acell = tableView.dequeueReusableCellWithIdentifier(CellConfig.NameCell, forIndexPath: indexPath) as! BaseInfomationCell
                
                acell.titleLabel.text = "Name"
                
                if let name  = info.objectForKey("formalName") as? String{
                    acell.valueLabel.textColor = UIColor.darkTextColor()
                    acell.valueLabel.text =  name
                    
                }else{
                    acell.valueLabel.textColor = UIColor.lightGrayColor()
                    acell.valueLabel.text =  "Tap to let people know how they can call you"
                }
                
                cell = acell
            case 2:

                let acell = tableView.dequeueReusableCellWithIdentifier(CellConfig.PhoneCell, forIndexPath: indexPath) as! BaseInfomationCell
                acell.titleLabel.text = "Phone"
       
                if let phone  = info.objectForKey("phone") as? String{
                    acell.valueLabel.textColor = UIColor.darkTextColor()
                    acell.valueLabel.text =  phone
                    
                }else{
                    acell.valueLabel.textColor = UIColor.lightGrayColor()
                    acell.valueLabel.text =  "Tap to add your phone"
                }
                
                cell = acell
            case 3:
                let acell = tableView.dequeueReusableCellWithIdentifier(CellConfig.BioCell, forIndexPath: indexPath) as! BioCell
                acell.titleLabel.text = "Bio"
                acell.valueLabel.text  = info.objectForKey("bio") as? String ?? "Tap to change your Bio"
                cell = acell
            default:
                break
            }
        
            
        }else{
            let acell = tableView.dequeueReusableCellWithIdentifier("RegisterDojoCell", forIndexPath: indexPath)
            cell = acell
        }
        
        
        return cell
    }
    

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if indexPath.section == 0{
            switch indexPath.row{
            case 0:
                return 60
            default:
                return UITableViewAutomaticDimension
            }
        }else{
            return 50
        }
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
         return UITableViewAutomaticDimension
    }

    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
        if indexPath.section == 1{
            let cvc = CreateDojoViewController()
            

            
            showViewController(cvc, sender: self)
        }else{
            
            PFUser.currentUser()?.objectForKey("info")?.fetchIfNeededInBackgroundWithBlock({ (info, error) -> Void in
                
                if let info = info{
                    
                    PKHUD.sharedHUD.hide(animated: true, completion: nil)
                    
                    
                    let vc =  EditProfileViewController()
                    vc.userInfo = info
                    
                    switch indexPath.row{
                    case 0:
                        vc.buildCallback = {(form)->(BaseRow) in
                            return ImageRow(){
                                $0.title = "Profile Photo"
                                //$0.value = info.objectForKey("picture")  as? String
                            }
                        }
                        
                        vc.submitCallback = { (row,info) -> () in
                            if let row = row as? ImageRow{
                                if let value = row.value{
                                    
                                    if let photo = self.uploadPhoto(value){
                                        
                                        info.setObject(photo, forKey: "picture")
                                    }
                                    
                                }
                            }
                        }
                    case 1:
                        vc.buildCallback = {(form)->(BaseRow) in
                            return NameRow() {
                                $0.title = "Name"
                                $0.value = info.objectForKey("formalName")  as? String
                            }
                        }
                        vc.submitCallback = { (row,info) -> () in
                            if let row = row as? NameRow{
                                if let value = row.value{
                                    info.setObject(value, forKey: "formalName")
                                }
                            }
                        }
                    case 2:

                        vc.buildCallback = {(form)->(BaseRow) in
                            return PhoneRow(){
                                $0.title = "Phone"
                                $0.value = info.objectForKey("phone")  as? String
                            }
                        }
                        vc.submitCallback = { (row,info) -> () in
                            if let row = row as? PhoneRow{
                                if let value = row.value{
                                    info.setObject(value, forKey: "phone")
                                }
                            }
                        }
                    case 3:
                        vc.buildCallback = {(form)->(BaseRow) in
                            return TextAreaRow(){
                                $0.placeholder = "Martial Artist Biography"
                                $0.value = info.objectForKey("bio")  as? String
                            }
                        }
                        
                        vc.submitCallback = { (row,info) -> () in
                            if let row = row as? TextAreaRow{
                                if let value = row.value{
                                     info.setObject(value, forKey: "bio")
                                }
                            }
                        }
                        
                        
                    default:
                        break
                    }
                    
                    vc.hidesBottomBarWhenPushed = true
                    self.showViewController(vc, sender: self)
                    
                }else{
                    
                    PKHUD.sharedHUD.contentView = PKHUDErrorView()
                    PKHUD.sharedHUD.hide(afterDelay: 0.5)
                }
            })
            

        }
    }
    
    func uploadPhoto(photo:UIImage)->PFObject?{
        
        let image = photo.resizedImageWithContentMode(.ScaleAspectFit, bounds: CGSizeMake(560, 560), interpolationQuality: .Default)
        
        let thumbnail = image.thumbnailImage(86, transparentBorder: 0, cornerRadius: 0, interpolationQuality: .Default)
        
        let imageData = UIImageJPEGRepresentation(image, 0.8)
        let thumbData = UIImagePNGRepresentation(thumbnail)
        
        guard imageData != nil && thumbData != nil else{
            return nil
        }
        
        let photoFile = PFFile(data: imageData!)
        let thumbFile = PFFile(data: thumbData!)
        
        
        let photo = PFObject(className: "Photo")
        photo["image"] = photoFile
        photo["thumbnail"] = thumbFile
        photo["user"] = PFUser.currentUser()
        
        
        return photo
    }
}

extension ProfileTableViewController:DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        if let _ = self.user{
            
            return nil
        }else{
            
            return NSAttributedString(string: "Get an account to start your dojo journey.", attributes: [NSForegroundColorAttributeName:UIColor.darkGrayColor(),NSFontAttributeName:UIFont.systemFontOfSize(30)])
        }

    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        if let _ = self.user{
            
            return nil
        }else{
            return NSAttributedString(string: "Login/Sign up", attributes: [NSFontAttributeName:UIFont.systemFontOfSize(22),NSForegroundColorAttributeName:UIColor(red: 18.0/255, green: 150.0/255, blue: 255.0/255, alpha: 1)])
        }
  
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        
        
        let vc = LoginViewController()
        vc.emailAsUsername = true
        vc.delegate = self
        vc.signUpController?.delegate = self
        
        showDetailViewController(vc, sender: self)
    }
    
}

extension ProfileTableViewController:PFSignUpViewControllerDelegate,PFLogInViewControllerDelegate {
    

    
    func logInViewController(logInController: PFLogInViewController, didLogInUser user: PFUser) {

        dismissViewControllerAnimated(true) { () -> Void in
            self.reloadUI()
        }
    }
    


    
    func signUpViewController(signUpController: PFSignUpViewController, didSignUpUser user: PFUser) {
        dismissViewControllerAnimated(true) { () -> Void in
            self.reloadUI()
        }
    }
    

}

class BioCell: UITableViewCell {    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
}
class BaseInfomationCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
}

class ProfileCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var profileView: PictureView!
}
