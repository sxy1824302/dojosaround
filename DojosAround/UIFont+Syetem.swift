//
//  UIFont+Syetem.swift
//  DojosAround
//
//  Created by alexander sun on 11/19/15.
//  Copyright © 2015 The Arsonist. All rights reserved.
//

import Foundation
import UIKit

extension UIFont{
    static func defaultFont(size:CGFloat)->UIFont?{
        
        //let fontname = UIFont.fontNamesForFamilyName("Chalkduster").first
  
        return  UIFont.systemFontOfSize(size)
    }
}