//
//  HelpTableViewController.swift
//  DojosAround
//
//  Created by alexander sun on 1/11/16.
//  Copyright © 2016 The Arsonist. All rights reserved.
//

import UIKit
import MessageUI

class HelpTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
        tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let mvc =  MFMailComposeViewController()
        mvc.mailComposeDelegate = self
        mvc.setToRecipients(["contact@ijubar.cn"])
        
        
        if indexPath.row == 0{
            mvc.setSubject("Dojos Around Feedback")
        }else{
            mvc.setSubject("Claim My Dojo")
        }
        
        showViewController(mvc, sender: self)
    }


    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

}

extension HelpTableViewController:MFMailComposeViewControllerDelegate{
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
