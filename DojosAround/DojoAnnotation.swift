//
//  DojoAnnotation.swift
//  DojosAround
//
//  Created by alexander sun on 11/16/15.
//  Copyright © 2015 The Arsonist. All rights reserved.
//

import UIKit
import MapKit
import Parse

class DojoAnnotation: NSObject,MKAnnotation {
    var dojo:PFObject!
    
    
    static func annotation(mapView:MKMapView,annotation:MKAnnotation)->MKAnnotationView{
        var view = mapView.dequeueReusableAnnotationViewWithIdentifier("DojoAnnotation")
        
        if view == nil{
            let aview  =  MKPinAnnotationView(annotation: annotation, reuseIdentifier: "DojoAnnotation")
            aview.pinColor = .Red
            aview.animatesDrop = true
            aview.canShowCallout = true
            view = aview
            
            
        }
        
        return view!
    }
    
    init(aDojo:PFObject) {
        dojo = aDojo
        super.init()
    }
    var coordinate: CLLocationCoordinate2D {
        
        if let location = dojo.objectForKey("location") as? PFGeoPoint{
             return CLLocationCoordinate2DMake(location.latitude ,location.longitude )
        }
        
        return CLLocationCoordinate2DMake(0, 0)
    }
    

    var title: String? {
        let location = dojo.objectForKey("name") as? String
        
        return location ?? "Name"
    }
    var subtitle: String? {
        let info = dojo.objectForKey("classInformation") as? String
        
        return info ?? "Not Specified"
    }
    
    override func isEqual(object: AnyObject?) -> Bool {
        return self.title == object?.title
    }
    

}

