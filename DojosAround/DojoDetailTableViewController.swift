//
//  DojoDetailTableViewController.swift
//  DojosAround
//
//  Created by alexander sun on 12/24/15.
//  Copyright © 2015 The Arsonist. All rights reserved.
//

import UIKit
import Parse
import PKHUD
class DojoNameCell: UITableViewCell {
    
    @IBOutlet weak var nameCell: UILabel!
}

class DojoDetailTableViewController: UITableViewController {

    var dojo:PFObject!
    var userInfo:PFObject?
    override func viewDidLoad() {
        super.viewDidLoad()

      
    }
    

    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
 
        if let _ = userInfo{
            return 2
        }
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 5
        }else{
            return 2
        }
        
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell!

        if indexPath.section == 0{
            switch(indexPath.row){
            case 0:
                cell = tableView.dequeueReusableCellWithIdentifier("NameCell", forIndexPath: indexPath)
                
                let aCell = cell as! DojoNameCell
                aCell.nameCell.text = dojo?.objectForKey("name") as? String
            case 1:
                cell = tableView.dequeueReusableCellWithIdentifier("InfoCell", forIndexPath: indexPath)
                
                cell.textLabel!.text =  "School"
                
                if let info = dojo?.objectForKey("school") as? String{
                    cell.detailTextLabel!.text = info
                    
                }else{
                    cell.detailTextLabel!.text = "Not Provided"
                }
            case 2:
                cell = tableView.dequeueReusableCellWithIdentifier("InfoCell", forIndexPath: indexPath)
                
                cell.textLabel!.text =  "Website"
                
                if let info = dojo?.objectForKey("website") as? String{
                    cell.detailTextLabel!.text =  info
                }else{
                    cell.detailTextLabel!.text = "Not Provided"
                }
            case 3:
                cell = tableView.dequeueReusableCellWithIdentifier("InfoCell", forIndexPath: indexPath)
                
                cell.textLabel!.text =  "Dojo Phone"
                
                if let info = dojo?.objectForKey("phone") as? String{
                    cell.detailTextLabel!.text =  info
                }else{
                    cell.detailTextLabel!.text = "Not Provided"
                }
            case 4:
                cell = tableView.dequeueReusableCellWithIdentifier("LongInfoCell", forIndexPath: indexPath)
                
                cell.textLabel!.text =  "Class Infomation"
                
                if let info = dojo?.objectForKey("classInformation") as? String{
                    cell.detailTextLabel!.text = info
                }else{
                    cell.detailTextLabel!.text = "Not Provided"
                }
                
            default:
                break
            }

        }else{
            switch(indexPath.row){
            case 0:
                cell = tableView.dequeueReusableCellWithIdentifier("InfoCell", forIndexPath: indexPath)
                
                cell.textLabel!.text =  "Phone"
                
                if let info = userInfo?.objectForKey("phone") as? String{
                    cell.detailTextLabel!.text =  info
                }else{
                    cell.detailTextLabel!.text = "Not Provided"
                }
            case 1:
                cell = tableView.dequeueReusableCellWithIdentifier("InfoCell", forIndexPath: indexPath)
                
                cell.textLabel!.text =  "Contactor"
                
                if let info = userInfo?.objectForKey("formalName") as? String{
                    cell.detailTextLabel!.text =  info
                }else{
                    cell.detailTextLabel!.text = "Not Provided"
                }
            default:
                break
            }

        }
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
